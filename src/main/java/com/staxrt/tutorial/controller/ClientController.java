package com.staxrt.tutorial.controller;

import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.Client;
import com.staxrt.tutorial.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * The type User controller.
 *
 */
@RestController
@RequestMapping("/api/v3")
public class ClientController {

  @Autowired
  private ClientRepository clientRepository;

  /**
   * Get all users list.
   *
   * @return the list
   */
  @GetMapping("/clients")
  public List<Client> getAllClients() {
    return clientRepository.findAll();
  }

  /**
   * Gets users by id.
   *
   * @param userId the user id
   * @return the users by id
   * @throws ResourceNotFoundException the resource not found exception
   */
  @GetMapping("/clients/{id}")
  public ResponseEntity<Client> getClientsById(@PathVariable(value = "id") Long clientId)
      throws ResourceNotFoundException {
    Client client =
        clientRepository
            .findById(clientId)
            .orElseThrow(() -> new ResourceNotFoundException("Client not found on :: " + clientId));
    return ResponseEntity.ok().body(client);
  }
  
  @GetMapping("/getclient")
  public List<Client> ClientAuth(@RequestParam(value="email") String email, @RequestParam(value="pass") String pass) {
      //return userRepository.findByName(name);
	  List<Client> clients = clientRepository.findByEmail(email);
	  List<Client> ret = new Vector<Client>();
	  for(Client i : clients) {
		  if (i.getPass().equals(pass)) {
			  ret.add(i);
		  }
	  }
	  return ret;
  }
  
  @GetMapping("/getclient-post")
  public Client getClientByPostId(@RequestParam(value="postId") Long postId) {
	  return clientRepository.findByPostsId(postId);
  }
  
  /**
   * Create user user.
   *
   * @param user the user
   * @return the user
   */
  @PostMapping("/clients")
  public Client createUser(@Valid @RequestBody Client user) {
    return clientRepository.save(user);
  }

//  /**
//   * Update user response entity.
//   *
//   * @param userId the user id
//   * @param userDetails the user details
//   * @return the response entity
//   * @throws ResourceNotFoundException the resource not found exception
//   */
//  @PutMapping("/users/{id}")
//  public ResponseEntity<Client> updateUser(
//      @PathVariable(value = "id") Long userId, @Valid @RequestBody Client clientDetails)
//      throws ResourceNotFoundException {
//
//    Client client =
//        clientRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    client.setEmail(clientDetails.getEmail());
//    client.setName(clientDetails.getName());
//    client.setPass(clientDetails.getPass());
//    client.setCompanyName(clientDetails.getCompanyName());
//    client.setCompanyField(clientDetails.getCompanyField());
//    //user.setUpdatedAt(new Date());
//    final Client updatedClient = clientRepository.save(client);
//    return ResponseEntity.ok(updatedClient);
//  }
//
//  /**
//   * Delete user map.
//   *
//   * @param userId the user id
//   * @return the map
//   * @throws Exception the exception
//   */
//  @DeleteMapping("/user/{id}")
//  public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long clientId) throws Exception {
//    Client client =
//        clientRepository
//            .findById(clientId)
//            .orElseThrow(() -> new ResourceNotFoundException("Client not found on :: " + clientId));
//
//    clientRepository.delete(client);
//    Map<String, Boolean> response = new HashMap<>();
//    response.put("deleted", Boolean.TRUE);
//    return response;
//  }
}
