
package com.staxrt.tutorial.controller;

import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.WatchlistDetail;
import com.staxrt.tutorial.repository.PostRepository;
import com.staxrt.tutorial.repository.WatchlistRepository;
import com.staxrt.tutorial.repository.WatchlistDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * The type User controller.
 *
 */
@RestController
@RequestMapping("/api/v3")
public class WatchlistDetailController {

  @Autowired
  private WatchlistRepository watchlistRepository;
  
  @Autowired
  private WatchlistDetailRepository watchlistDetailRepository;
  
  @Autowired
  private PostRepository postRepository;

  /**
   * Get all users list.
   *
   * @return the list
   */
  @GetMapping("/watchlist-detail")
  public List<WatchlistDetail> getAllWatchlistDetail() {
    return watchlistDetailRepository.findAll();
  }
  
  @GetMapping("/watchlist-detail/{id}")
  public ResponseEntity<WatchlistDetail> getWatchlistById(@PathVariable(value = "id") Long watchlistDetailId)
      throws ResourceNotFoundException {
    WatchlistDetail watchlistDetail =
        watchlistDetailRepository
            .findById(watchlistDetailId)
            .orElseThrow(() -> new ResourceNotFoundException("WatchlistDetail not found on :: " + watchlistDetailId));
    return ResponseEntity.ok().body(watchlistDetail);
  }
//  /**
//   * Gets users by id.
//   *
//   * @param userId the user id
//   * @return the users by id
//   * @throws ResourceNotFoundException the resource not found exception
//   */
//  @GetMapping("/watchlist/{id}")
//  public ResponseEntity<WatchlistDetail> getDetailByWatchlistId(@PathVariable(value = "watchlistId") Long watchlistId)
//      throws ResourceNotFoundException {
//    WatchlistDetail watchlistDetail =
//        watchlistDetailRepository
//            .findById(watchlistId)
//            .orElseThrow(() -> new ResourceNotFoundException("WatchlistDetail not found on :: " + watchlistId));
//    return ResponseEntity.ok().body(watchlistDetail);
//  }
  
  @GetMapping("/getwatchlist-detail-1")
  public List<WatchlistDetail> getWatchlistDetailByPostId(@RequestParam(value="postId") Long postId) {
	  return watchlistDetailRepository.findByPostId(postId);
  }
  
  @GetMapping("/getwatchlist-detail-2")
  public List<WatchlistDetail> getWatchlistDetailByWatchlistId(@RequestParam(value="watchlistId") Long watchlistId) {
	  return watchlistDetailRepository.findByWatchlistId(watchlistId);
  }
  
  /**
   * Create user user.
   *
   * @param user the user
   * @return the user
   * @throws ResourceNotFoundException 
   */
  @PostMapping("/watchlist-detail")
  public WatchlistDetail createWatchlistDetail(@RequestParam(value="watchlistId") Long watchlistId, 
		  @RequestParam(value="postId") Long postId, @Valid @RequestBody WatchlistDetail watchlistDetail) throws ResourceNotFoundException {
	  watchlistRepository.findById(watchlistId).map(watchlist -> {
		  watchlistDetail.setWatchlist(watchlist);
		  return true;
	  }).orElseThrow(()-> new ResourceNotFoundException("WatchlistId " + watchlistId + " not found"));
	  
	  postRepository.findById(postId).map(post -> {
		  watchlistDetail.setPost(post);
		  return true;
	  }).orElseThrow(()-> new ResourceNotFoundException("PostId " + postId + " not found"));
	  
	  return watchlistDetailRepository.save(watchlistDetail);
  }

  /**
   * Update user response entity.
   *
   * @param userId the user id
   * @param userDetails the user details
   * @return the response entity
   * @throws ResourceNotFoundException the resource not found exception
   */
//  @PutMapping("/users/{id}")
//  public ResponseEntity<User> updateUser(
//      @PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails)
//      throws ResourceNotFoundException {
//
//    User user =
//        userRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    user.setEmail(userDetails.getEmail());
//    user.setName(userDetails.getName());
//    user.setPass(userDetails.getPass());
//    //user.setUpdatedAt(new Date());
//    final User updatedUser = userRepository.save(user);
//    return ResponseEntity.ok(updatedUser);
//  }
//
//  /**
//   * Delete user map.
//   *
//   * @param userId the user id
//   * @return the map
//   * @throws Exception the exception
//   */
//  @DeleteMapping("/user/{id}")
//  public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws Exception {
//    User user =
//        userRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    userRepository.delete(user);
//    Map<String, Boolean> response = new HashMap<>();
//    response.put("deleted", Boolean.TRUE);
//    return response;
//  }
}
