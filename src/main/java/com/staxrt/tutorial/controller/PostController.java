package com.staxrt.tutorial.controller;

import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.Post;
import com.staxrt.tutorial.repository.ClientRepository;
import com.staxrt.tutorial.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * The type User controller.
 *
 */
@RestController
@RequestMapping("/api/v3")
public class PostController {

  @Autowired
  private PostRepository postRepository;
  
  @Autowired
  private ClientRepository clientRepository;

  /**
   * Get all users list.
   *
   * @return the list
   */
  @GetMapping("/posts")
  public List<Post> getAllPosts() {
    return postRepository.findAll();
  }

  /**
   * Gets users by id.
   *
   * @param userId the user id
   * @return the users by id
   * @throws ResourceNotFoundException the resource not found exception
   */
  @GetMapping("/posts/{id}")
  public ResponseEntity<Post> getPostsById(@PathVariable(value = "id") Long postId)
      throws ResourceNotFoundException {
    Post post =
        postRepository
            .findById(postId)
            .orElseThrow(() -> new ResourceNotFoundException("Post not found on :: " + postId));
    return ResponseEntity.ok().body(post);
  }
  
  @GetMapping("/getpost")
  public List<Post> getPostByClientId(@RequestParam(value="clientId") Long clientId) {
      //return userRepository.findByName(name);
	  return postRepository.findByClientId(clientId);
  }
  
  @GetMapping("/getpost-detail")
  public Post getPostByWatchlistDetailId(@RequestParam(value="watchlistDetailId") Long WatchlistDetailId) {
	  return postRepository.findByWatchlistDetailsId(WatchlistDetailId);
  }
  
  /**
   * Create user user.
   *
   * @param user the user
   * @return the user
   * @throws ResourceNotFoundException 
   */
  @PostMapping("/posts")
  public Post createPost(@RequestParam(value="clientId") Long clientId, 
		  @Valid @RequestBody Post post) throws ResourceNotFoundException {
	  return clientRepository.findById(clientId).map(client -> {
		  post.setClient(client);
		  return postRepository.save(post);
	  }).orElseThrow(()-> new ResourceNotFoundException("ClientId " + clientId + " not found"));
  }

  /**
   * Update user response entity.
   *
   * @param userId the user id
   * @param userDetails the user details
   * @return the response entity
   * @throws ResourceNotFoundException the resource not found exception
   */
//  @PutMapping("/users/{id}")
//  public ResponseEntity<User> updateUser(
//      @PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails)
//      throws ResourceNotFoundException {
//
//    User user =
//        userRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    user.setEmail(userDetails.getEmail());
//    user.setName(userDetails.getName());
//    user.setPass(userDetails.getPass());
//    //user.setUpdatedAt(new Date());
//    final User updatedUser = userRepository.save(user);
//    return ResponseEntity.ok(updatedUser);
//  }
//
//  /**
//   * Delete user map.
//   *
//   * @param userId the user id
//   * @return the map
//   * @throws Exception the exception
//   */
//  @DeleteMapping("/user/{id}")
//  public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws Exception {
//    User user =
//        userRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    userRepository.delete(user);
//    Map<String, Boolean> response = new HashMap<>();
//    response.put("deleted", Boolean.TRUE);
//    return response;
//  }
}
