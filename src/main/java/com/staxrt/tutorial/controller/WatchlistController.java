package com.staxrt.tutorial.controller;

import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.Watchlist;
import com.staxrt.tutorial.repository.UserRepository;
import com.staxrt.tutorial.repository.WatchlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * The type User controller.
 *
 */
@RestController
@RequestMapping("/api/v3")
public class WatchlistController {

  @Autowired
  private WatchlistRepository watchlistRepository;
  
  @Autowired
  private UserRepository userRepository;

  /**
   * Get all users list.
   *
   * @return the list
   */
  @GetMapping("/watchlist")
  public List<Watchlist> getAllPosts() {
    return watchlistRepository.findAll();
  }

  /**
   * Gets users by id.
   *
   * @param userId the user id
   * @return the users by id
   * @throws ResourceNotFoundException the resource not found exception
   */
  @GetMapping("/watchlist/{id}")
  public ResponseEntity<Watchlist> getWatchlistById(@PathVariable(value = "id") Long watchlistId)
      throws ResourceNotFoundException {
    Watchlist watchlist =
        watchlistRepository
            .findById(watchlistId)
            .orElseThrow(() -> new ResourceNotFoundException("Watchlist not found on :: " + watchlistId));
    return ResponseEntity.ok().body(watchlist);
  }
  
  @GetMapping("/getwatchlist-user")
  public Watchlist getWatchlistByUserId(@RequestParam(value="userId") Long userId) {
      //return userRepository.findByName(name);
	  return watchlistRepository.findByUserId(userId);
  }
  
  @GetMapping("/getwatchlist-from-detail")
  public Watchlist getWatchlistByWatchlistDetailId(@RequestParam(value="watchlistDetailId") Long watchlistDetailId) {
      //return userRepository.findByName(name);
	  return watchlistRepository.findByWatchlistDetailsId(watchlistDetailId);
  }
  
  /**
   * Create user user.
   *
   * @param user the user
   * @return the user
   * @throws ResourceNotFoundException 
   */
  @PostMapping("/watchlist")
  public Watchlist createWatchlist(@RequestParam(value="userId") Long userId, 
		  @Valid @RequestBody Watchlist watchlist) throws ResourceNotFoundException {
	  return userRepository.findById(userId).map(user -> {
		  watchlist.setUser(user);
		  return watchlistRepository.save(watchlist);
	  }).orElseThrow(()-> new ResourceNotFoundException("UserId " + userId + " not found"));
  }

  /**
   * Update user response entity.
   *
   * @param userId the user id
   * @param userDetails the user details
   * @return the response entity
   * @throws ResourceNotFoundException the resource not found exception
   */
//  @PutMapping("/users/{id}")
//  public ResponseEntity<User> updateUser(
//      @PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails)
//      throws ResourceNotFoundException {
//
//    User user =
//        userRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    user.setEmail(userDetails.getEmail());
//    user.setName(userDetails.getName());
//    user.setPass(userDetails.getPass());
//    //user.setUpdatedAt(new Date());
//    final User updatedUser = userRepository.save(user);
//    return ResponseEntity.ok(updatedUser);
//  }
//
//  /**
//   * Delete user map.
//   *
//   * @param userId the user id
//   * @return the map
//   * @throws Exception the exception
//   */
//  @DeleteMapping("/user/{id}")
//  public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws Exception {
//    User user =
//        userRepository
//            .findById(userId)
//            .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
//
//    userRepository.delete(user);
//    Map<String, Boolean> response = new HashMap<>();
//    response.put("deleted", Boolean.TRUE);
//    return response;
//  }
}
