package com.staxrt.tutorial.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Client.
 *
 */
@Entity
@Table(name = "client_apps")
@EntityListeners(AuditingEntityListener.class)
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email_address", nullable = false)
    private String email;
    
    @Column(name = "password", nullable = false) 
    private String pass;
    
    @Column(name = "company_name", nullable = false) 
    private String companyName;
    
    @Column(name = "company_field", nullable = false) 
    private String companyField;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "client")
    private List<Post> posts = new ArrayList<Post>();
    
    public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyField() {
		return companyField;
	}

	public void setCompanyField(String companyField) {
		this.companyField = companyField;
	}

/**
   * Gets id.
   *
   * @return the id
   */
  public long getId() {
        return id;
    }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(long id) {
        this.id = id;
    }
  
  public void setName(String name) {
	  this.name = name;
  }
  
  public String getName() {
	  return name;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
        return email;
    }

  /**
   * Sets email.
   *
   * @param email the email
   */
  public void setEmail(String email) {
        this.email = email;
    }
  
  public void setPass(String p) {
	  this.pass = p;
  }
  
  public String getPass() {
	  return pass;
  }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password = '" + pass + '\'' +
                ", companyName = '" + companyName + '\'' +
                ", companyField = '" + companyField + '\'' +
                '}';
    }


}
