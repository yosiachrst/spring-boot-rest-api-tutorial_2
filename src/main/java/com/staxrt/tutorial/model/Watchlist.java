package com.staxrt.tutorial.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Watchlist.
 *
 */
@Entity
@Table(name = "watchlist")
@EntityListeners(AuditingEntityListener.class)
public class Watchlist {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "watchlist")
    private List<WatchlistDetail> watchlistDetails = new ArrayList<WatchlistDetail>();

    public List<WatchlistDetail> getWatchlistDetails() {
		return watchlistDetails;
	}

	public void setWatchlistDetails(List<WatchlistDetail> watchlistDetails) {
		this.watchlistDetails = watchlistDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

/**
   * Gets id.
   *
   * @return the id
   */
	public long getId() {
        return id;
    }

  /**
   * Sets id.
   *
   * @param id the id
   */
	public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Watchlist{" +
                "id=" + id +
                '}';
    }


}
