package com.staxrt.tutorial.repository;

import com.staxrt.tutorial.model.Client;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface User repository.
 *
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	List<Client> findByEmail(String email);
	Client findByPostsId(Long postId);
}
