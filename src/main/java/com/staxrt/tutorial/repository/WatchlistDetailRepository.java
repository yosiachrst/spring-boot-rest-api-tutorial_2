package com.staxrt.tutorial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.staxrt.tutorial.model.Post;
import com.staxrt.tutorial.model.Watchlist;
import com.staxrt.tutorial.model.WatchlistDetail;

/**
 * The interface User repository.
 *
 */
@Repository
public interface WatchlistDetailRepository extends JpaRepository<WatchlistDetail, Long> {
	List<WatchlistDetail> findByWatchlistId(Long watchlistId);
	List<WatchlistDetail> findByPostId(Long postId);
}

