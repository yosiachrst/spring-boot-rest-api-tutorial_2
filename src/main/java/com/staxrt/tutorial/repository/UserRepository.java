package com.staxrt.tutorial.repository;

import com.staxrt.tutorial.model.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface User repository.
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	List<User> findByEmail(String email);
	User findByWatchlistId(Long watchlistId);
}
