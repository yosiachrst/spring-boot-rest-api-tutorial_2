package com.staxrt.tutorial.repository;

import com.staxrt.tutorial.model.Post;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface User repository.
 *
 */
@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
	List<Post> findByClientId(Long clientId);
	Post findByWatchlistDetailsId(Long watchlistDetailId);
}
