package com.staxrt.tutorial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.staxrt.tutorial.model.Post;
import com.staxrt.tutorial.model.Watchlist;

/**
 * The interface User repository.
 *
 */
@Repository
public interface WatchlistRepository extends JpaRepository<Watchlist, Long> {
	Watchlist findByUserId(Long userId);
	Watchlist findByWatchlistDetailsId(Long watchlistDetailId);
}
